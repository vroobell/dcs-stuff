redIADS = SkynetIADS:create('my_iads')
redIADS:addSAMSitesByPrefix('SAM')
redIADS:addEarlyWarningRadarsByPrefix('EW')
--first get the SAM site you want to use as point defence from the IADS:
local sa15 = redIADS:getSAMSiteByGroupName('SAM-tor')
--then add it to the SAM site it should protect:
redIADS:getSAMSiteByGroupName('SAM-hawk'):addPointDefence(sa15)
redIADS:activate()