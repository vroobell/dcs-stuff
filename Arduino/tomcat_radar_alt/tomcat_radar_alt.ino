
#define DCSBIOS_IRQ_SERIAL
#include "DcsBios.h"
#include <LiquidCrystal.h>

//LCD pin to Arduino
const int pin_RS = 8;
const int pin_EN = 9;
const int pin_d4 = 4;
const int pin_d5 = 5;
const int pin_d6 = 6;
const int pin_d7 = 7;
const int pin_BL = 10;
LiquidCrystal lcd( pin_RS,  pin_EN,  pin_d4,  pin_d5,  pin_d6,  pin_d7);

void onIasUsChange(char* newValue) {
  lcd.setCursor(0, 0);
  lcd.print(String(newValue) + "kts");
}

DcsBios::StringBuffer<4> iasUsBuffer(0x0426, onIasUsChange);

void onAltMslFtChange(unsigned int newValue) {
  lcd.setCursor(9, 0);
  lcd.print("       ");
  lcd.setCursor(9, 0);
  lcd.print(String(newValue) + "ft");
}
DcsBios::IntegerBuffer altMslFtBuffer(0x0432, 0xffff, 0, onAltMslFtChange);

void onHdgDegChange(unsigned int newValue) {
  lcd.setCursor(0, 1);
  lcd.print(String(newValue));
  lcd.print((char)223);
}
DcsBios::IntegerBuffer hdgDegBuffer(0x0434, 0x01ff, 0, onHdgDegChange);

void onPltRadaraltiNeedleChange(unsigned int newValue) {
  lcd.setCursor(9, 1);
  lcd.print("       ");
  lcd.setCursor(9, 1);
  lcd.print(String(round(getRdrAlt(newValue))) + "B");
}
DcsBios::IntegerBuffer pltRadaraltiNeedleBuffer(0x12fc, 0xffff, 0, onPltRadaraltiNeedleChange);

void setup() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Jebac PiS");
  delay(2000);
  DcsBios::setup();

  lcd.clear();
}

void loop() {
  DcsBios::loop();
}

float getRdrAlt(unsigned int newValue) {
  if (newValue > 55277) {
    return 0;
  } else if (newValue > 34506) 
    return -0.0153* newValue + 923;
  else if (newValue > 24769) {
   return -0.0597 * newValue + 2477;
  }
  else {
    return -0.262*newValue + 7734;
  }
}
