#define DCSBIOS_IRQ_SERIAL
#include "DcsBios.h"
#include "U8glib.h"

U8GLIB_NHD_C12864 u8g(13, 11, 10, 9, 8);
unsigned int irot, bingo, alt, rdrot, jat, flaps  = 0;

void loop() {
  DcsBios::loop();
}

float getSpeed(unsigned int newValue) {
  if (newValue < 20000)
    return 0.00655 * newValue + 58.6;
  else if (newValue < 34000) {
    return 0.00831 * newValue + 18.2;
  }
  else {
    return 0.0108 * newValue - 65.5;
  }
}

void onPltAirspeedNeedleChange(unsigned int newValue) {
  float speed = getSpeed(newValue);
  int blocks = (int)speed / 3;
  u8g.firstPage();
  do {
    u8g.drawFrame(0, 0, 128, 20);
    u8g.drawBox(0, 0, blocks, 20);
    u8g.setFont(u8g_font_baby);
    u8g.drawStr(33, 25, "'");
    u8g.drawStr(28, 27, "100");
    u8g.drawStr(50, 25, "'");
    u8g.drawStr(66, 25, "'");
    u8g.drawStr(60, 27, "200");
    u8g.drawStr(83, 25, "'");
    u8g.drawStr(100, 25, "'");
    u8g.drawStr(94, 27, "300");
    //    char tmp_string1[32];
    //    u8g.drawStr(33, 50, itoa(speed, tmp_string1, 10));
    //
    //    char tmp_string2[32];
    //    u8g.drawStr(66, 50, itoa(newValue, tmp_string2, 10));

     if (alt == 1) {
      u8g.drawStr(0, 36, "ALT!");
    } else {
      u8g.drawStr(0, 36, "    ");
    }

    if (bingo == 1) {
      u8g.drawStr(0, 46, "BINGO");
    } else {
      u8g.drawStr(0, 46, "     ");
    }

    if (flaps == 1) {
      u8g.drawStr(0, 56, "FLAPS!");
    } else {
      u8g.drawStr(0, 56, "     ");
    }

    if (irot == 1) {
      u8g.drawStr(64, 36, "IROT");
    } else {
      u8g.drawStr(64, 36, "    ");
    }

    if (rdrot == 1) {
      u8g.drawStr(64, 46, "RDROT");
    } else {
      u8g.drawStr(64, 46, "     ");
    }   

    if (jat == 1) {
      u8g.drawStr(64, 56, "JAT");
    } else {
      u8g.drawStr(64, 56, "   ");
    }

  } while ( u8g.nextPage() );
}

DcsBios::IntegerBuffer pltAirspeedNeedleBuffer(0x1302, 0xffff, 0, onPltAirspeedNeedleChange);
void setup() {
  u8g.setContrast(0);
  u8g.setRot180();
  DcsBios::setup();

  u8g.firstPage();
  do {
    u8g.drawFrame(0, 0, 128, 20);
    u8g.setFont(u8g_font_baby);
    u8g.drawStr(33, 25, "'");
    u8g.drawStr(28, 27, "100");
    u8g.drawStr(50, 25, "'");
    u8g.drawStr(66, 25, "'");
    u8g.drawStr(60, 27, "200");
    u8g.drawStr(83, 25, "'");
    u8g.drawStr(100, 25, "'");
    u8g.drawStr(94, 27, "300");

  } while ( u8g.nextPage() );
}

void onPltRadarAltLightChange(unsigned int newValue) {
  alt = newValue;
}
DcsBios::IntegerBuffer pltRadarAltLightBuffer(0x12e0, 0x0008, 3, onPltRadarAltLightChange);

void onPltWarnBingoChange(unsigned int newValue) {
  bingo = newValue;
}
DcsBios::IntegerBuffer pltWarnBingoBuffer(0x12da, 0x4000, 14, onPltWarnBingoChange);

void onPltWarnFlapChange(unsigned int newValue) {
  flaps = newValue;
}
DcsBios::IntegerBuffer pltWarnFlapBuffer(0x12dc, 0x0800, 11, onPltWarnFlapChange);

void onRioDddLightsIrotChange(unsigned int newValue) {
  irot = newValue;
}
DcsBios::IntegerBuffer rioDddLightsIrotBuffer(0x12ec, 0x8000, 15, onRioDddLightsIrotChange);

void onRioDddLightsRdrotChange(unsigned int newValue) {
  rdrot = newValue;
}
DcsBios::IntegerBuffer rioDddLightsRdrotBuffer(0x12ec, 0x2000, 13, onRioDddLightsRdrotChange);

void onRioDddLightsJatChange(unsigned int newValue) {
  jat = newValue;
}
DcsBios::IntegerBuffer rioDddLightsJatBuffer(0x12ec, 0x4000, 14, onRioDddLightsJatChange);
