
#define DCSBIOS_IRQ_SERIAL
#include "DcsBios.h"
#include <LiquidCrystal.h>

//LCD pin to Arduino
const int pin_RS = 8;
const int pin_EN = 9;
const int pin_d4 = 4;
const int pin_d5 = 5;
const int pin_d6 = 6;
const int pin_d7 = 7;
const int pin_BL = 10;
LiquidCrystal lcd( pin_RS,  pin_EN,  pin_d4,  pin_d5,  pin_d6,  pin_d7);

void onIasUsChange(char* newValue) {
  lcd.setCursor(0, 0);
  lcd.print(String(newValue) + "kts");
}

DcsBios::StringBuffer<4> iasUsBuffer(0x0426, onIasUsChange);

void onAltMslFtChange(unsigned int newValue) {
  lcd.setCursor(9, 0);
  lcd.print("       ");
  lcd.setCursor(9, 0);
  if (newValue < 10000)  {
    lcd.print("0");
  }

  lcd.print(String(newValue) + "ft");
}
DcsBios::IntegerBuffer altMslFtBuffer(0x0432, 0xffff, 0, onAltMslFtChange);

void onHdgDegChange(unsigned int newValue) {
  lcd.setCursor(0, 1);
  if (newValue < 100)  {
    lcd.print("0");
  }
  lcd.print(String(newValue));
  lcd.print((char)223);
}
DcsBios::IntegerBuffer hdgDegBuffer(0x0434, 0x01ff, 0, onHdgDegChange);

void onPltFuelTotalDispChange(unsigned int newValue) {
  String bars = "";
  int two_thousands = newValue / 2000;
  lcd.setCursor(5, 1);
  lcd.print("       ");
  lcd.setCursor(5, 1);
  if (two_thousands <= 7) {
    for (int i = 1; i <= two_thousands; i++) {
      lcd.print("_");
    }
  }
  else {
    lcd.print("_______");
  }
}
DcsBios::IntegerBuffer pltFuelTotalDispBuffer(0x138a, 0xffff, 0, onPltFuelTotalDispChange);

void onPltRadarAltLightChange(unsigned int newValue) {

  lcd.setCursor(13, 1);
  if (newValue == 1) {
    lcd.print("A");
  }
  else lcd.print(" ");
}
DcsBios::IntegerBuffer pltRadarAltLightBuffer(0x12e0, 0x0008, 3, onPltRadarAltLightChange);

void onPltSpdbrkFullLightChange(unsigned int newValue) {
  lcd.setCursor(14, 1);
  if (newValue == 1) {
    lcd.print("B");
  }
  else lcd.print(" ");
}
DcsBios::IntegerBuffer pltSpdbrkFullLightBuffer(0x12e0, 0x0100, 8, onPltSpdbrkFullLightChange);

void onPltFlapsIndLightChange(unsigned int newValue) {
  lcd.setCursor(15, 1);
  if (newValue == 1) {
    lcd.print("F");
  }
  else lcd.print(" ");
}
DcsBios::IntegerBuffer pltFlapsIndLightBuffer(0x12e0, 0x0040, 6, onPltFlapsIndLightChange);

void setup() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
  lcd.print("Jebac PiS");
  delay(2000);
  DcsBios::setup();

  lcd.clear();
}

void loop() {
  DcsBios::loop();
}
